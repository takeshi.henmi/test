package com.example.myapplication

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import java.util.regex.Pattern

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(setOf(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications))
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    fun convertVersionStr2Int(version: String?): Int {
        /**
         * バージョン番号の文字列を比較用のIntへ変換する
         * 各番号は3桁まで許容する
         * 区切りの「.」が2個未満の場合は0を返す
         * 例:
         * 1.2.3    ->  1002003
         * 22.1.333 -> 22001333
         * 1.2      ->        0
         */
        if (version == null) {
            return 0
        }

        val versionList = version.split(Pattern.quote(".").toRegex()).dropLastWhile { it.isEmpty() }
            .toTypedArray()
        if (versionList.size < 3) {
            return 0
        }

        var versionInt = 0
        try {
            versionInt = (Integer.parseInt(versionList[0]) * 1000000
                    + Integer.parseInt(versionList[1]) * 1000
                    + Integer.parseInt(versionList[2]))
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        }

        return versionInt
    }
}