package com.example.myapplication

import org.junit.Test

import org.junit.Assert.*
import org.mockito.Mockito

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    val splashActivityMock = Mockito.mock(MainActivity::class.java)

    @Test
    fun convertVersionStr2IntTestNullParameter() {
        Mockito.`when`(splashActivityMock.convertVersionStr2Int(null)).thenReturn(0)
        assertEquals(0, splashActivityMock.convertVersionStr2Int(null))
    }

    @Test
    fun convertVersionStr2IntTest() {
        Mockito.`when`(splashActivityMock.convertVersionStr2Int("1.5")).thenReturn(1005)
        assertEquals(1005, splashActivityMock.convertVersionStr2Int("1.5"))
    }
}